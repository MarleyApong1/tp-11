<?php

namespace Tests;

use Marley\Tp11\Salarie;
use PHPUnit\Framework\TestCase;

class SalarieTest extends TestCase
{
    public function testSetMatricule()
    {
        $salarie = new Salarie();
        $salarie->setMatricule(1234);
        $this->assertEquals(1234, $salarie->getMatricule());
    }

    public function testGetDateEmbauche()
    {
        $dateEmbauche = "01/01/2020";
        $salarie = new Salarie(0, "", 0, $dateEmbauche);
        $this->assertEquals($dateEmbauche, $salarie->getDateEmbauche()->format("m/d/Y"));
    }

    public function testExperience()
    {
        $dateEmbauche = (new \DateTime())->modify('-5 years')->format("m/d/Y");
        $salarie = new Salarie(0, "", 0, $dateEmbauche);
        $this->assertEquals(5, $salarie->experience());
    }

    public function testCalculerSalaireNet()
    {
        $salarie = new Salarie(0, "", 2000);
        $this->assertEquals(1600, $salarie->calculerSalaireNet());
    }

    public function testPrimeAnnuelle()
    {
        $dateEmbauche = (new \DateTime())->modify('-5 years')->format("m/d/Y");
        $salarie = new Salarie(0, "", 2000, $dateEmbauche);
        $this->assertEquals(2100, $salarie->primeAnnuelle());  // Correction ici
    }
}
